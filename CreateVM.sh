VBoxManage createvm --name "service" --register
VBoxManage modifyvm "service" --memory 4096 --acpi on --boot1 dvd
VBoxManage modifyvm "service" --nic1 hostonly --hostonlyadapter1 vboxnet0 --nicpromisc1 allow-all
VBoxManage modifyvm "service" --nic2 nat  --natnet2 "192.168/16" --natpf2 "guestssh,tcp,,2222,,22" --nicpromisc2 allow-all
VBoxManage modifyvm "service" --cpus 3
VBoxManage modifyvm "service" --ostype Linux

VBoxManage createhd --filename /mnt/vdis/io.vdi --size 10000
VBoxManage storagectl "service" --name "IDE Controller" --add ide
VBoxManage storageattach "service" --storagectl "IDE Controller"  \
    --port 0 --device 0 --type hdd --medium /mnt/vdis/io.vdi
VBoxManage storageattach "service" --storagectl "IDE Controller" \
    --port 1 --device 0 --type dvddrive --medium /mnt/git/roscloudinit/ros.iso

VBoxManage createvm --name "service1" --register
VBoxManage modifyvm "service1" --memory 4096 --acpi on --boot1 dvd
VBoxManage modifyvm "service1" --nic1 hostonly --hostonlyadapter1 vboxnet0 --nicpromisc1 allow-all
VBoxManage modifyvm "service1" --nic2 nat  --natnet2 "192.168/16" --natpf2 "guestssh,tcp,,2222,,22" --nicpromisc2 allow-all
VBoxManage modifyvm "service1" --cpus 3
VBoxManage modifyvm "service1" --ostype Linux

VBoxManage createhd --filename /mnt/vdis/io1.vdi --size 10000
VBoxManage storagectl "service1" --name "IDE Controller" --add ide
VBoxManage storageattach "service1" --storagectl "IDE Controller"  \
    --port 0 --device 0 --type hdd --medium /mnt/vdis/io1.vdi
VBoxManage storageattach "service1" --storagectl "IDE Controller" \
    --port 1 --device 0 --type dvddrive --medium /mnt/git/roscloudinit/ros.iso

VBoxManage createvm --name "service2" --register
VBoxManage modifyvm "service2" --memory 4096 --acpi on --boot1 dvd
VBoxManage modifyvm "service2" --nic1 hostonly --hostonlyadapter1 vboxnet0 --nicpromisc1 allow-all
VBoxManage modifyvm "service2" --nic2 nat  --natnet2 "192.168/16" --natpf2 "guestssh,tcp,,2222,,22" --nicpromisc2 allow-all
VBoxManage modifyvm "service2" --cpus 2
VBoxManage modifyvm "service2" --ostype Linux

VBoxManage createhd --filename /mnt/vdis/io2.vdi --size 10000
VBoxManage storagectl "service2" --name "IDE Controller" --add ide
VBoxManage storageattach "service2" --storagectl "IDE Controller"  \
    --port 0 --device 0 --type hdd --medium /mnt/vdis/io2.vdi
VBoxManage storageattach "service2" --storagectl "IDE Controller" \
    --port 1 --device 0 --type dvddrive --medium /mnt/git/roscloudinit/ros.iso
