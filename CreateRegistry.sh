#!/bin/bash

docker pull index.alauda.cn/library/registry:2.4.1
docker pull index.alauda.cn/library/nginx
docker tag index.alauda.cn/library/registry:2.4.1 registry:2.4.1
docker tag index.alauda.cn/library/nginx nginx
docker rmi index.alauda.cn/library/registry:2.4.1
docker rmi index.alauda.cn/library/nginx

docker network create registry

docker run -it -d --name dockerregistry --net registry registry:2.4.1

docker run -it -d --name nginxregistry --net registry -p 443:443 -v /mnt/git/mesos-rancher/nginx.conf:/etc/nginx/conf.d/default.conf -v /mnt/git/mesos-rancher/f.crt:/f.crt -v /mnt/git/mesos-rancher/f.key:/f.key nginx

docker pull index.alauda.cn/philo/mesos-single-docker
docker tag index.alauda.cn/philo/mesos-single-docker afafaf.newb.xyz/mesos-single-docker
docker push afafaf.newb.xyz/mesos-single-docker
